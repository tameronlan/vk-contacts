/* external code require */
let gzipStatic = require('connect-gzip-static');
let express = require("express");
let cookieParser = require("cookie-parser");
let app = express();

/* my code require */
let Router = require(__dirname + '/core/Router');
let appInfo = require(__dirname + '/core/config');

Object.assign(global, {
    appInfo: appInfo,
    app: app,
    appPath: __dirname
});

/* engine for render 'ejs'. More info: http://www.embeddedjs.com/ */
app.set('view engine', "ejs");
app.disable('x-powered-by');

/* supporting cookie */
app.use(cookieParser());

/* gzip for static files. More info: https://www.npmjs.com/package/connect-gzip-static */
app.use(gzipStatic(appPath + '/public/static'));

/* static url like /static/base/img will be translate to __dirname + '/public/static/base/img' */
app.use('/static', express.static(appPath + '/public/static'));

/* custom router for searching Controller, action for request */
Router.do();

/* listen port */
app.listen(appInfo.port);

console.log("Listening on port " + appInfo.port);