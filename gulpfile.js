var gulp = require('gulp');
var sass = require('gulp-sass');
var replace = require('gulp-replace');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var runSequence = require('run-sequence');
var spritesmith = require('gulp.spritesmith');
var cssImageDimensions = require('gulp-css-image-dimensions');

var sassInput = ['public/static/css/*.scss', 'public/static/css/*/*.scss', 'public/static/css/*/*/*.scss'];
var sassOptions = {
    errLogToConsole: true
};

gulp.task('sass', function() {
    return gulp.src(sassInput, { base: '.' })
        .pipe(sourcemaps.init())
        .pipe(cssImageDimensions('/*'))
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['> 1%', 'Opera > 12', 'IE >= 10'],
            cascade: false
        }))
        .pipe(sourcemaps.write('.', {
            includeContent: false
        }))
        .pipe(gulp.dest('.'));
});

gulp.task('watch', function() {
    gulp.watch(sassInput, ['sass']);
});

gulp.task('default', function() {
    runSequence(['sass', 'watch']);
});
