import Viewer from 'Viewer';
import DomHelper from 'DomHelper';

class AppContacts {
  viewers = {};
  store = {};

  constructor(opts = {}){
    var {
      apiAlias = 'vk',
      appId = false,
      data = {}
    } = opts;

    if ( !appId ){
      console.log('appId is not defined')
    }

    this.store = new Store(data);
    this.api = new ExternalApiVk();
    this.viewers = {};

    this.run();
  }

  run(){
    console.log('app is run')
  }

  isAuth(){

  }

  auth(){

  }
}

class ExternalApiVk {
  constructor(){

  }

  isAuth(){

  }

  auth(){

  }

  getCurrentUser(){

  }
}

class Helper {

}

class Store{
  constructor(opts = {}){

  }

  setData(data = {}){

  }

  deleteData(opts = {}){

  }

  addEvent(){

  }

  rmEvent(){

  }
}