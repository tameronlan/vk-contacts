let BaseController = require(`${appPath}/core/Controller`);

class MainController extends BaseController{
  constructor(props){
    super(props);
  }

  index(){
    this
      .setTitle('Контакты')
      .setData({ activeNavTab: 'contacts' })
      .addMeta({ name: 'description', content: '' })
      .addStyle({ src: '/static/css/base.css', })
      .addScript({ src: 'https://vk.com/js/api/openapi.js?159', external: 1, })
      .render("index");
  }

  about(){
    this
      .setTitle('Обо мне')
      .setData({ activeNavTab: 'about' })
      .addStyle({ src: '/static/css/about/base.css', })
      .render("about");
  }
}

module.exports = MainController;