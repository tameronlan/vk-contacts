let BaseController = require(`${appPath}/core/Controller`);

class GameController extends BaseController{
  constructor(props){
    super(props);
  }

  index(){
    this
      .setTitle('Выберите игру', false)
      .addMeta({ name: 'description', content: '' })
      .addStyle({ src: '/static/css/game/base.css'})
      .render("index",{
        layout: 'game'
      });
  }

  countFigures(){
    this
      .setTitle('Игра. Сколько фигур нарисовано', false)
      .addMeta({ name: 'description', content: '' })
      .addStyle({ src: '/static/css/game/count-figures.css'})
      .addScript({ src: '/static/js/game/count-figures.js'})
      .render("countFigures",{
        layout: 'game'
      });
  }
}

module.exports = GameController;