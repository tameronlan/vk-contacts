let fs = require('fs');

class Router{
  static do(){
    // set root catalog
    app.all("/", (request, result, next)=>{
      request.params.controllerPath = 'main';

      Router.getRoute(request, result, next);
    });

    // set route for controller/controller/action urls
    app.all("/:controllerPath/:controllerName/:action", Router.getRoute);

    // set route for controller/action urls
    app.all("/:controllerPath/:action", Router.getRoute);

    // set route for only controller
    app.all("/:controllerPath/", Router.getRoute);

    app.all("*", Router.notFound);
  }

  static getRoute(request, result, next){
    let controllerPath = request.params.controllerPath;
    let controllerName = request.params.controllerName || 'Index';
    let actionName = request.params.action || 'index';
    let pathSingle = `${appPath}/app/${controllerPath}/Controller.js`;
    let pathFolder = `${appPath}/app/${controllerPath}/controller/${controllerName}.js`;
    let paths = [pathSingle, pathFolder];
    let controller = false;

    for (let path of paths){
      if ( fs.existsSync(path) ) {
        controller = new (require(path))({
          request,
          result,
          path: controllerPath
        });

        break;
      }
    }

    if ( controller ){
      if ( controller[actionName] ){
        controller[actionName]();
      } else {
        Router.notFound(request, result, next);
      }
    } else {
      next();
    }
  }

  static notFound(request, result, next){
    request.params.controllerPath = 'error';
    request.params.action = 'error404';

    Router.getRoute(request, result, next);
  }
}

module.exports = Router;