class Controller {
  constructor(props = {}){
    this.dispatch(props);
  }

  dispatch(props = {}){
    this.request    = props.request;
    this.result     = props.result;
    this.next       = props.next;
    this.path       = props.path;
    this.data       = this.result.locals;
    this.meta       = [];
    this.styles     = [];
    this.scripts    = [];

    this.setData({
      title: '',
      activeTab: ''
    });

    app.set('views', `${appPath}/app/layout`);
  }

  render(action = false, props = {}){
    let {
      layout = 'web'
    } = props;

    console.log(action, props);

    this.setData({
      pagePath: false,
    });

    if ( action ){
      this.setData({
        pagePath: [appPath, 'app', this.path, 'tpl', action].join('/'),
      });
    }

    this.data.meta      = this.meta;
    this.data.styles    = this.styles;
    this.data.scripts   = this.scripts;

    this.data.action    = action;

    this.result.render(layout);
  }

  addMeta(opts = {}){
    let {
      name = false
    } = opts;

    if ( !name ){
      return;
    }

    this.meta.push(opts);

    return this;
  }

  addStyle(opts){
    let {
      src = false
    } = opts;

    if ( !src ){
      return;
    }

    this.styles.push(opts);

    this.styles.sort((f,s) => {
      return (f.priority || 0) - (s.priority || 0);
    });

    return this;
  }

  addScript(opts){
    let {
      src = false
    } = opts;

    if ( !src ){
      return;
    }

    this.scripts.push(opts);

    this.scripts.sort((f,s) => {
      return (f.priority || 0) - (s.priority || 0);
    });

    return this;
  }

  setTitle( pageTitle  = '', withCommonText = true ){
    let commonText = '. Телефонный справочник';
    let title = `${pageTitle}${ (withCommonText ? commonText : '' )}`;

    this.setData({
      title: title
    });

    return this;
  }

  setData( data = {} ){
    Object.assign(this.data, data);

    return this;
  }
}

module.exports = Controller;