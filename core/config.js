let fs = require('fs');
let environment = {};

if ( fs.existsSync('core/environment.json') ) {
    environment = require('./environment.json');
}

const STATE_APP_PRODUCTION = 'prod';
const STATE_APP_STAGE = 'stage';
const STATE_APP_LOCAL = 'local';

module.exports = {
    mysql: {
        host: '127.0.0.1',
        port: 2000,
        user: '',
        name: ''
    },
    state: STATE_APP_LOCAL,
    port: environment.port ? environment.port : 80,
    isProduction(){
        return this.state === STATE_APP_PRODUCTION;
    },
    isLocal(){
        return this.state === STATE_APP_LOCAL;
    },
    isStage(){
        return this.state === STATE_APP_STAGE;
    },
    getStates(){
        return [STATE_APP_PRODUCTION, STATE_APP_LOCAL, STATE_APP_STAGE];
    },
    'STATE_APP_PRODUCTION': STATE_APP_PRODUCTION,
    'STATE_APP_STAGE': STATE_APP_STAGE,
    'STATE_APP_LOCAL': STATE_APP_LOCAL,
};