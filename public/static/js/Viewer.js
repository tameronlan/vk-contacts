import DomHelper from 'DomHelper';

class Viewer{
  constructor(opts = {}){
    if ( opts.data ){
      this.dataIsFunction = _h.isFunction(opts.data);
      this.data = opts.data;
    }

    this.beforeMount = opts.beforeMount ? opts.beforeMount : () => {};
    this.afterMount = opts.afterMount ? opts.afterMount : () => {};

    if ( opts.tplId ){
      this.tplId = opts.tplId;
      this.needGetTpl = opts.tplId;
    } else {
      this.tpl = opts.tpl || '';
    }

    if ( opts.render && _h.isFunction(opts.render) ){
      this.render = opts.render;
    }

    if ( opts.do && _h.isFunction(opts.do) ){
      this.do = opts.do || function(){};
    }

    this.deps = opts.deps || [];

    Viewer.saveInstance(this);
  }

  render(opts = {}){
    var data = opts.data ? opts.data : this.dataIsFunction ? this.data() : this.data;
    var tpl = _h.tpl(this.getTpl());

    return tpl(data);
  }

  /**
   * handlers execute before mount with handlers of deps
   */
  beforeMountQueue(){
    let used = {};

    if ( this.beforeMount ) {
      this.beforeMount();

      used[this.id] = true;
    }

    if ( this.deps ){
      for( let dep of this.deps ){
        if ( dep && dep.id && !used[dep.id] ){
          dep.beforeMount();

          used[this.id] = true;
        }
      }
    }
  }

  /**
   * handlers execute after mount with handlers of deps
   */
  afterMountQueue(){
    let used = {};

    if ( this.afterMount ) {
      this.afterMount();

      used[this.id] = true;
    }

    if ( this.deps ){
      for( let dep of this.deps ){
        if ( dep && dep.id && !used[dep.id] ){
          dep.afterMount();

          used[this.id] = true;
        }
      }
    }
  }

  mount(opts={}){
    this.beforeMountQueue();
    if ( this.do ) this.do(opts);
    this.afterMountQueue();

    return this;
  }

  getTpl(){
    return this.needGetTpl ? Viewer.getTpl(this.tplId) : this.tpl;
  }

  /**
   * @returns {{}|*}
   */
  static getTplsCache(){
    if ( !this.tplsCache ){
      this.tplsCache = {};
    }

    return this.tplsCache;
  }

  /**
   *  method for get tpl  string by id
   * @param id
   * @param ignoreCache
   * @returns {*}
   */
  static getTpl(id = '', ignoreCache = false){
    let tplsCache = Viewer.getTplsCache();
    let in_cache = !ignoreCache ? tplsCache.hasOwnProperty(id) : false;

    if ( in_cache ){
      return tplsCache[id];
    } else {
      let res_ge = _q.byId(id);

      if ( !res_ge ){
        return '';
      } else {
        tplsCache[id] = res_ge.innerHTML;

        return tplsCache[id];
      }
    }
  }

  static saveInstance(instance){
    if ( !this._instances ){
      this._instances = [];
    }

    instance.id = this._instances.length;

    this._instances.push(instance);
  }
}


export default Viewer;