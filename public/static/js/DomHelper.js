class DomHelper{
  static gid(id){
    return document.getElementById(id);
  }

  static qs(selector, scope = false){
    return (scope || document).querySelector(selector);
  }

  static qsa(selector, scope){
    return (scope || document).querySelectorAll(selector);
  }
}

export default DomHelper;